package com.jafariarman.myapplication.utils;

public class Constants
{
    //request code for checking location settings
    public static int REQUEST_CHECK_SETTINGS = 1000;

    //request code for add marker activity
    public static int REQUEST_ADD_MARKER = 1001;
}
