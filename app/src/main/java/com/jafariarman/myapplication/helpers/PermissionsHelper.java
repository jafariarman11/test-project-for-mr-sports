package com.jafariarman.myapplication.helpers;

import android.Manifest;
import android.content.Context;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.jafariarman.myapplication.R;
import com.jafariarman.myapplication.views.MainActivity;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;

public class PermissionsHelper
{
    public interface PermissionCallback
    {
        void onGranted();
        void onDenied();
    }

    public static void requestLocationPermission(Context context, PermissionCallback permissionCallback)
    {
        Dexter.withActivity((AppCompatActivity)context).withPermissions(Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION).withListener(new MultiplePermissionsListener() {
            @Override
            public void onPermissionsChecked(MultiplePermissionsReport report)
            {
                if (report.areAllPermissionsGranted())
                {
                    permissionCallback.onGranted();
                }
                else
                {
                    permissionCallback.onDenied();
                }
            }

            @Override
            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token)
            {
                token.continuePermissionRequest();
            }
        }).withErrorListener(new PermissionRequestErrorListener()
        {
            @Override
            public void onError(DexterError error)
            {
                String msg = context.getResources().getString(R.string.error_in_get_location_permission);
                Toast.makeText(context,msg, Toast.LENGTH_SHORT).show();
            }
        }).onSameThread().check();
    }
}
