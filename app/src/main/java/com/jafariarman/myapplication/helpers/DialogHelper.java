package com.jafariarman.myapplication.helpers;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jafariarman.myapplication.R;

public class DialogHelper
{
    /**
     * class for handle custom progress dialog
     */
    public static class ProgressDialog
    {
        private Context context;
        private String message;
        private Dialog progress_dialog;

        public ProgressDialog(Context context) {
            this.context = context;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public void show()
        {
            progress_dialog = new Dialog(context);
            progress_dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
            progress_dialog.setContentView(R.layout.custom_progress_dialog_layout);
            progress_dialog.setCancelable(false);

            TextView message_textview = progress_dialog.findViewById(R.id.progress_dialog_tv);

            message_textview.setText(message);

            progress_dialog.show();
            //remove shadow background in back of dialog
            progress_dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        }


        public void dismiss() {
            progress_dialog.dismiss();
        }

    }

    /**
     * custom class for alert dialog
     */
    public static class MaterialAlertDialog
    {
        private AlertDialog.Builder builder;
        Dialog dialog;
        private Context context;

        View content_view;

        private ImageView header_icon;
        private LinearLayout header_layout,title_layout;

        private Button positive_btn,negative_btn;
        private TextView title_tv,message_tv,header_title;

        public interface OnPositiveClickListener
        {
            void onClick();
        }

        public interface OnNegativeClickListener
        {
            void onClick();
        }

        public interface OnDismissListener
        {
            void onDismiss();
        }

        public MaterialAlertDialog(Context context)
        {
            this.context = context;

            builder = new AlertDialog.Builder(context);

            content_view = LayoutInflater.from(context).
                    inflate(R.layout.material_dialog_layout,null);

            builder.setView(content_view);

        }

        public void setCancelable(boolean cancelable)
        {
            builder.setCancelable(cancelable);
        }

        public void setTitle(String title)
        {
            title_tv = content_view.findViewById(R.id.title_tv);
            title_layout = content_view.findViewById(R.id.title_lyt);
            title_layout.setVisibility(View.VISIBLE);

            title_tv.setText(title);
        }

        public void setMessage(String message)
        {
            message_tv = content_view.findViewById(R.id.message_tv);
            message_tv.setText(message);
        }


        public void setCustomView(View view)
        {
            builder.setView(view);
        }

        public void setPositive_btn(String text,OnPositiveClickListener onPositiveClickListener)
        {
            positive_btn = content_view.findViewById(R.id.positive_btn);

            positive_btn.setVisibility(View.VISIBLE);
            positive_btn.setText(text);

            positive_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    if (onPositiveClickListener != null)
                    {
                        onPositiveClickListener.onClick();
                    }
                }
            });
        }

        public void setNegative_btn(String text,OnNegativeClickListener onNegativeClickListener)
        {
            negative_btn = content_view.findViewById(R.id.negative_btn);

            negative_btn.setVisibility(View.VISIBLE);
            negative_btn.setText(text);

            negative_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view)
                {
                    if (onNegativeClickListener != null)
                    {
                        onNegativeClickListener.onClick();
                    }
                }
            });
        }

        public void show()
        {
            dialog = builder.create();
            dialog.show();
        }

        public Dialog getDialog()
        {
            return dialog;
        }

        public void dismiss()
        {
            dialog.dismiss();
        }

        public void setOnDismissListener(OnDismissListener onDismissListener)
        {
            if (onDismissListener != null)
            {
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        onDismissListener.onDismiss();
                    }
                });
            }
        }

    }


}
