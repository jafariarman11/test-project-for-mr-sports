package com.jafariarman.myapplication.views;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.jafariarman.myapplication.R;

public class InsertActivity extends AppCompatActivity
{

    private EditText titleEt,descEt;
    private LatLng latLng;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_insert);

        latLng = (LatLng) getIntent().getExtras().get("lat_lng");

        initViews();
    }

    private void initViews()
    {
        titleEt = findViewById(R.id.titleEt);
        descEt = findViewById(R.id.descEt);
    }

    /**
     * finish this activity and pass the values to the previous activity for inserting marker
     * @param view
     */
    public void insert(View view)
    {
        String title = titleEt.getText().toString();
        String desc = descEt.getText().toString();

        if (!title.equals("") && !desc.equals("")) // if values not empty
        {
            Intent resultIntent = new Intent();
            resultIntent.putExtra("title",title);
            resultIntent.putExtra("desc",desc);
            resultIntent.putExtra("lat_lng",latLng);

            setResult(RESULT_OK,resultIntent);
            finish();
        }
        else
        {
            Toast.makeText(this,getResources().getString(R.string.fill_all_fields),
                    Toast.LENGTH_SHORT).show();
        }
    }


    public void onBackBtnClick(View view)
    {
        finish();
    }

}