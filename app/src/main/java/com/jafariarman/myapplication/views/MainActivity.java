package com.jafariarman.myapplication.views;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationAvailability;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.jafariarman.myapplication.R;
import com.jafariarman.myapplication.helpers.DialogHelper;
import com.jafariarman.myapplication.helpers.PermissionsHelper;

import static com.jafariarman.myapplication.utils.Constants.REQUEST_ADD_MARKER;
import static com.jafariarman.myapplication.utils.Constants.REQUEST_CHECK_SETTINGS;

public class MainActivity extends AppCompatActivity implements OnSuccessListener,OnFailureListener
{
    private MapView mapView;
    private GoogleMap googleMap;
    private View google_location_btn;
    private FloatingActionButton fab_map_style;

    // bunch of location related apis
    private FusedLocationProviderClient mFusedLocationClient;
    private SettingsClient mSettingsClient;
    private LocationRequest mLocationRequest;
    private LocationSettingsRequest mLocationSettingsRequest;
    private LocationCallback mLocationCallback;
    private Location mCurrentLocation;

    // location updates interval - 10sec
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    // fastest updates interval - 5 sec
    // location updates will be received if another app is requesting the locations
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = 5000;

    private DialogHelper.ProgressDialog progressDialog;

    private int mapType = GoogleMap.MAP_TYPE_NORMAL;


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CHECK_SETTINGS && resultCode == RESULT_OK)
        {
            getLocationUpdate();
        }

        if (requestCode == REQUEST_ADD_MARKER && resultCode == RESULT_OK)
        {
            String title = data.getExtras().getString("title");
            String desc = data.getExtras().getString("desc");
            LatLng latLng = (LatLng) data.getExtras().get("lat_lng");

            addMarker(latLng,title,desc);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initLocationService();

        initViews();
        initComponents();

        mapView.onCreate(savedInstanceState);

    }

    private void initViews()
    {
        mapView = findViewById(R.id.map);
        fab_map_style = findViewById(R.id.fab_map_style);
    }

    private void initComponents()
    {
        getMap();
    }


    private void getMap()
    {

        mapView.getMapAsync(googleMap ->
        {
            this.googleMap = googleMap;
            googleMap.setMapType(mapType); //set map default type
            initMapListeners();

            checkPermissions();
        });
    }

    private void initMapListeners()
    {
        //set on map click listener
        googleMap.setOnMapClickListener(latLng ->
        {
            //open insert activity for adding marker with title and description
            Intent insertActivity = new Intent(this,InsertActivity.class);
            insertActivity.putExtra("lat_lng",latLng);
            startActivityForResult(insertActivity,REQUEST_ADD_MARKER);
        });
    }

    /**
     * check location permissions
     */
    private void checkPermissions()
    {
        PermissionsHelper.requestLocationPermission(this, new PermissionsHelper.PermissionCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onGranted()
            {
                googleMap.setMyLocationEnabled(true);
                hideGoogleLocationBtn();
                checkLocationSettings();
            }

            @Override
            public void onDenied()
            {
                permissionDeniedDialog();
            }
        });
    }


    /**
     * hide google default user location button (because we use our's :D )
     */
    @SuppressLint("ResourceType")
    private void hideGoogleLocationBtn()
    {
        View f = (View) mapView.findViewById(1).getParent();
        google_location_btn = f.findViewById(2);
        google_location_btn.setVisibility(View.GONE);
    }


    /**
     * show user location on map
     */
    private void showUserLocation()
    {
        moveMapCamera(mCurrentLocation.getLatitude(),mCurrentLocation.getLongitude(),10);
    }

    /**
     * move map camera to a custom location with custom zoom
     */
    private void moveMapCamera(double lat,double lng,int zoom)
    {
        LatLng latLng = new LatLng(lat,lng);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, zoom);
        googleMap.animateCamera(cameraUpdate);
    }

    /**
     * initialize location service
     */
    private void initLocationService()
    {
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mSettingsClient = LocationServices.getSettingsClient(this);

        //this callback should be called when location data has come
        mLocationCallback = new LocationCallback()
        {
            @Override
            public void onLocationResult(LocationResult locationResult)
            {
                if (locationResult.getLastLocation() != null)
                {
                    mCurrentLocation = locationResult.getLastLocation();
                    stopLocationUpdate();
                    progressDialog.dismiss(); //dismiss get location progress dialog when location received
                    showUserLocation();
                }
            }
        };

        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        builder.addLocationRequest(mLocationRequest);
        mLocationSettingsRequest = builder.build();
    }

    /**
     * check device gps state and show dialog if not enabled
     */
    public void checkLocationSettings()
    {
        mSettingsClient.checkLocationSettings(mLocationSettingsRequest).addOnSuccessListener(this::onSuccess)
                .addOnFailureListener(this::onFailure);
    }

    /**
     * send location request to google api
     */
    @SuppressLint("MissingPermission")
    public void getLocationUpdate()
    {
        progressDialog = new DialogHelper.ProgressDialog(this);
        progressDialog.setMessage(getResources().getString(R.string.please_wait));
        progressDialog.show();

        mFusedLocationClient.requestLocationUpdates(mLocationRequest,mLocationCallback, Looper.myLooper());
    }

    /**
     * stop location update service
     */
    public void stopLocationUpdate()
    {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback).addOnCompleteListener(task ->
        {
            /* location update stopped */
        });
    }

    /**
     * show a error dialog to user for denying permissions
     */
    private void permissionDeniedDialog()
    {
        String title = getResources().getString(R.string.permission_error);
        String msg = getResources().getString(R.string.permission_need_msg);
        String pos_btn_txt = getResources().getString(R.string.view_permissions);
        String neg_btn_txt = getResources().getString(R.string.exit);

        DialogHelper.MaterialAlertDialog alertDialog = new DialogHelper.MaterialAlertDialog(this);

        alertDialog.setCancelable(false);
        alertDialog.setTitle(title);
        alertDialog.setMessage(msg);

        //check permissions again
        alertDialog.setPositive_btn(pos_btn_txt, () -> {
            checkPermissions();
            alertDialog.dismiss();
        });

        //exit app if user does't want to allow permissions
        alertDialog.setNegative_btn(neg_btn_txt,() -> {
            finish();
        });

        alertDialog.show();
    }

    /**
     * when user location setting is on
     * @param o
     */
    @Override
    public void onSuccess(Object o)
    {
        getLocationUpdate();
    }

    /**
     * when user location settings not on and turning on location task has been failed
     * @param e exeption
     */
    @Override
    public void onFailure(@NonNull Exception e)
    {
        int statusCode = ((ApiException) e).getStatusCode();
        switch (statusCode)
        {
            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                try
                {
                    // Show the dialog by calling startResolutionForResult(), and check the
                    // result in onActivityResult().
                    ResolvableApiException rae = (ResolvableApiException) e;
                    rae.startResolutionForResult(this,REQUEST_CHECK_SETTINGS);
                }
                catch (IntentSender.SendIntentException sie)
                {
                    Toast.makeText(this, "PendingIntent unable to execute request.", Toast.LENGTH_SHORT).show();
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                String errorMessage = "Location settings are inadequate, and cannot be " +
                        "fixed here. Fix in Settings.";

                Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
        }
    }


    /**
     * show user location button listener
     * @param view
     */
    public void onLocationBtnClick(View view)
    {
        if (mCurrentLocation != null)
        {
            showUserLocation();
        }
        else
        {
            checkLocationSettings();
        }
    }

    /**
     * this method change map type between normal and hybrid
     */
    public void changeMapType(View view)
    {
        if (mapType == GoogleMap.MAP_TYPE_NORMAL)
        {
            mapType = GoogleMap.MAP_TYPE_HYBRID;
            googleMap.setMapType(mapType);
            fab_map_style.setImageDrawable(getResources().getDrawable(R.drawable.ic_map_outline));
        }
        else
        {
            mapType = GoogleMap.MAP_TYPE_NORMAL;
            googleMap.setMapType(mapType);
            fab_map_style.setImageDrawable(getResources().getDrawable(R.drawable.ic_earth));
        }
    }

    /**
     * add marker to map
     * @param latLng location of marker  as LatLng
     * @param title title of marker as string
     * @param desc description of marker as string
     */
    private void addMarker(LatLng latLng,String title,String desc)
    {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(title);
        markerOptions.snippet(desc);
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));


        googleMap.addMarker(markerOptions);
        moveMapCamera(latLng.latitude,latLng.longitude,10);
    }

    @Override
    public void onPause()
    {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}